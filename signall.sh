# !/bin/bash
signscript="../ios-ipa-resign/sign.sh"
ipasourcefolder="Source"
ipadestfolder="../Destination/"

developer1="iPhone Developer: xxxx (xxxx)"
mobileprovision1="../mobileprovision"

bundleid="null.null" #use null.null if you want to use the default app bundleid


cd $ipasourcefolder
find -d . -type f -name "*.ipa"> files.txt
while IFS='' read -r line || [[ -n "$line" ]]; do
	filename=$(basename "$line" .ipa)
	echo "Ipa: $filename"
	#_dev1_______
	output=$ipadestfolder$filename
	output+="_signed_dev1.ipa"
	"$signscript" "$line" "$developer1" "$mobileprovision1" "$output" "$bundleid"
done < files.txt
rm files.txt